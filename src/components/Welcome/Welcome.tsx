import { Text } from '@tarojs/components'

import styles from './Welcome.module.scss'

interface WelcomeProps {
  msg: string
}

export default function Welcome({ msg }: WelcomeProps) {
  return <Text className={styles.welcome}>{msg}</Text>
}
