import { hideLoading, showLoading, useLoad } from '@tarojs/taro'
import { useState } from 'react'

export function useInit() {
  const [title, setTitle] = useState('- -')

  async function init() {
    showLoading({
      title: '加载中...'
    })

    setTimeout(() => {
      setTitle('Hello Taro')
      hideLoading()
    }, 1500)
  }

  useLoad(() => {
    init()
  })

  return {
    title
  }
}
