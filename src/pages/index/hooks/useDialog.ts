import { useState } from 'react'

export function useDialog() {
  const [show, setShow] = useState(false)

  function handleOpen() {
    setShow(true)
  }

  function handleClose() {
    setShow(false)
  }

  return {
    show,
    handleOpen,
    handleClose
  }
}
