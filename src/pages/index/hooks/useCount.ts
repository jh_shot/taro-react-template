import { useMainStore } from '@/store'

export function useCount() {
  const { increment, decrement, count } = useMainStore()

  function handleIncrement() {
    increment()
  }

  function handleDecrement() {
    decrement()
  }

  return {
    count,
    handleIncrement,
    handleDecrement
  }
}
