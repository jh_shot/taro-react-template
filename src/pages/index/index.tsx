import { Button, Popup, Space } from '@nutui/nutui-react-taro'
import { Image, Text, View } from '@tarojs/components'
import { navigateTo, useDidHide, useDidShow, useLoad } from '@tarojs/taro'

import { Welcome } from '@/components'
import logo from '@/static/logo.png'

import { useCount, useDialog, useInit } from './hooks'
import styles from './index.module.scss'

export default function Index() {
  const { count, handleDecrement, handleIncrement } = useCount()
  const { show, handleOpen, handleClose } = useDialog()
  const { title } = useInit()

  function handleJump() {
    navigateTo({ url: '/pages/other/index' })
  }

  useLoad(options => {
    console.log('page load', options)
  })

  useDidShow(options => {
    console.log('page show', options)
  })

  useDidHide(() => {
    console.log('page hide')
  })

  return (
    <View className={styles.view}>
      <View className={styles.logoWrap}>
        <Image className={styles.logo} src={logo} />
      </View>
      <View className={styles.textWrap}>
        <Welcome msg={title} />
        <Text>{count}</Text>
        <View className={styles.btn}>
          <Space>
            <Button type="primary" onClick={handleIncrement}>
              +
            </Button>
            <Button type="primary" onClick={handleDecrement}>
              -
            </Button>
          </Space>
        </View>
        <Button className={styles.btn} type="primary" onClick={handleOpen}>
          弹窗
        </Button>
        <Button className={styles.btn} type="primary" onClick={handleJump}>
          跳转
        </Button>
      </View>
      <Popup visible={show} closeable onClose={handleClose}>
        <View className={styles.dialog}>
          <View className={styles.content}>
            <Text>Hello World</Text>
          </View>
        </View>
      </Popup>
    </View>
  )
}
