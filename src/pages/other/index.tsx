import { Button } from '@nutui/nutui-react-taro'
import { View } from '@tarojs/components'
import { navigateBack } from '@tarojs/taro'

import styles from './index.module.scss'

export default function Other() {
  function handleBack() {
    navigateBack()
  }

  return (
    <View className={styles.view}>
      <Button type="primary" onClick={handleBack}>
        返回
      </Button>
    </View>
  )
}
