/**
 * @type {import('stylelint').Config}
 */
module.exports = {
  extends: ['stylelint-config-standard'],
  plugins: ['stylelint-prettier'],
  overrides: [
    {
      files: ['**/*.scss'],
      customSyntax: 'postcss-scss'
    }
  ],
  rules: {
    'prettier/prettier': true,
    'block-no-empty': null,
    'no-empty-source': null,
    'property-no-unknown': [true, { ignoreProperties: ['/composes/'] }],
    'selector-class-pattern': null,
    'selector-pseudo-class-no-unknown': null,
    'no-descending-specificity': null,
    'selector-type-no-unknown': [true, { ignoreTypes: ['/page/'] }],
    'selector-pseudo-element-no-unknown': [
      true,
      { ignorePseudoElements: ['input-placeholder'] }
    ]
  }
}
