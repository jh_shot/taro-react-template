# Taro React Template

## 简介

Taro React Template 是一个开源的小程序模版。

## 特性

- **技术栈**：使用 React/Taro3/NutUI 等前端前沿技术开发
- **TypeScript**：应用程序级 JavaScript 的语言

## 准备

- [Node](http://nodejs.org/) 和 [Git](https://git-scm.com/)
- [React](https://cn.vuejs.org/guide/introduction.html)
- [Taro3](https://taro-docs.jd.com/docs)
- [NutUI](https://nutui.jd.com/taro/react/2x/#/zh-CN/guide/intro-react)

## 安装使用

- 获取项目代码

```bash
git clone https://gitee.com/jh_shot/taro-react-template.git
```

- 安装依赖

```bash
pnpm i
```

- 运行

```bash
pnpm dev:weapp
```

- 打包

```bash
pnpm build:weapp
```

## 注意事项

- 使用 Axios 作为跨端网络请求库，上传需要用小程序自己的API

## 待处理

- [ ] React > 18.2.0，H5 控制台报错 [issue](https://github.com/jdf2e/nutui-react/issues/1488)
